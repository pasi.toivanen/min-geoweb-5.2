// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { ConfigurableMapConnect } from '@opengeoweb/core';

export function App() {
  return (
    <div>
      <h1>Testing opengeoweb</h1>
      <div>
        <ConfigurableMapConnect layers={[]}/>
      </div>
    </div>
  );
}

export default App;
